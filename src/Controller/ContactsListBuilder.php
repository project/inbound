<?php

namespace Drupal\inbound\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Contacts.
 */
class ContactsListBuilder extends ConfigEntityListBuilder {

    /**
     * {@inheritdoc}
     */
    public function buildHeader() {
        $header['label'] = $this->t('Contacts');
        return $header + parent::buildHeader();
    }

    /**
     * {@inheritdoc}
     */
    public function buildRow(EntityInterface $entity) {
        $row['label'] = $entity->getCreatedTime();
        // You probably want a few more properties here...
        return $row + parent::buildRow($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultOperations(EntityInterface $entity) {
        /** @var \Drupal\field\FieldConfigInterface $entity */
        $operations = parent::getDefaultOperations($entity);

        if (isset($operations['edit'])) {
            $operations['edit']['weight'] = 30;
        }

        return $operations;
    }

}
