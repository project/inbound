<?php

namespace Drupal\inbound\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\inbound\Entity\Contact;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class ContactController extends ControllerBase {
    function detail($id) {

        $contact = \Drupal::entityTypeManager()->getStorage('inbound_contact')->load($id);
        /*
        $apiController = new ApiController();
        $params = array('contact_id'=>$id);
        $events = $apiController->findEvent($params);


        foreach($events as $event) {
            $events_array[] = $event->toArray();
        }*/

        $events_array = array();

        $build = array(
            '#theme' => 'contact_detail',
            '#contact' => $contact->toArray(),
            '#events' => $events_array
        );

        return $build;
    }
}